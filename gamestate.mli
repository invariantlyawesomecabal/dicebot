(* A Gamestate.t represents an instance of Dudo. *)
type t

exception NotEnoughPlayers


(* [new_game pnames] is a new game with players as named in [pnames].
 * Raises NotEnoughPlayers if there are fewer than two distinct valid player names. *)
val new_game : string list -> t

(* [current_player gs] is the name of the current player. *)
val current_player : t -> string

(* [current_dice gs] gets the dice in the hand of the current player. *)
val current_dice : t -> int list

(* [current_estimate gs] gets the current estimate (n, m). *)
val current_estimate : t -> int*int

(* [player_names gs] gets the list of current players. *)
val player_names : t -> string list

(* [num_dice gs] gets the number of dice in play. *)
val num_dice : t -> int

(* [roll_dice gs] is [gs] with all dice rolled to new values. *)
val roll_dice : t -> t

(* [winner gs] is the name of the winner if any. *)
val winner : t -> string option

(* [estimate gs n m] is the game [gs] after the current player makes an estimate "n m's" and a message to be broadcasted to the players. *)
val estimate : t -> int -> int -> t * string

(* [call gs] is the game [gs] after the current player makes a call and a message to be broadcasted to the players. *)
val call : t -> t * string

(* [spoton gs] is the game [gs] after the current player makes a spot-on and a message to be broadcasted to the players. *)
val spoton : t -> t * string


(* [rename_player player newname] is [gs] with [player] renamed with [newname]. *)
val rename_player : t -> string -> string -> t

(* [remove_player gs player] is [gs] with [player] removed. *)
val remove_player : t -> string -> t


val remove_player_and_reset : t -> string -> t

(* [game_string gs] is a string representation of [gs] from [player]'s point of view. *)
val game_string : t -> string

(* [dice_string gs player] is a string representation of [player]'s dice. *)
val dice_string : t -> string -> string

(* [to_string gs] is a string representation of [gs] for debugging purposes. *)
val to_string : t -> string
