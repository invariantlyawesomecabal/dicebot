module C = Irc_client_unix
open C.Io

let host = ref ""
let port = ref 6667
let nick = ref ""
let channel = ref ""
let message = "Hello, world!  Hello awesome people."
let commandc = ref '!' (* Prefix for commands sent by users. *)

let find_command conn chan nick msg =
  let cmd_of,rest = if msg.[0] = !commandc then
    let ri = String.index (msg^" ") ' ' in
    (String.sub (msg^" ") 1 (ri - 1),
     String.sub (msg^" ") (ri + 1) (String.length msg - ri))
  else
    (" ",msg)
  in
  let call_cb cb = cb conn chan nick rest in
  let appl c = function (cmd, _, f, _) when cmd = c -> call_cb f | _ -> () in
  List.iter (appl (String.lowercase cmd_of)) !Engine.registered_callbacks 

let callback connection result =
  let open Irc_message in
  match result with
  | `Ok {command=JOIN _; prefix=Some who} ->
      print_endline (who ^ " has joined the channel.")
  | `Ok {command=PRIVMSG (target,data); prefix=Some who} ->
      Printf.printf "Got message: %s from %s to %s\n" data who target;
      find_command connection target who data;
      flush_all ()
  | `Ok {command=Other ("376",_); _} ->
      C.send_join ~connection ~channel:!channel >>= fun () ->
      C.send_privmsg ~connection ~target:!channel ~message
  | `Ok msg ->
      print_endline (to_string msg);
      List.iter (fun f -> f connection msg) !Engine.registered_handlers 
  | `Error error ->
      Printf.printf "Failed to parse because: %s" error

exception FailedToConnect

(* TODO exception handling of C.connect_by_name *)
let main () =
  let ccharstr = ref "" in
  let speclist = 
    [("-server", Arg.Set_string host, "Sets the IRC server.");
     ("-port", Arg.Set_int port, "Sets the connection port.");
     ("-nick", Arg.Set_string nick, "Sets the nickname.");
     ("-channel", Arg.Set_string channel, "Sets the channel");
     ("-cmdchar", Arg.Set_string ccharstr, "Command prefix character.")] in
  Arg.parse speclist 
    (fun _ -> print_endline "No anonymous args allowed."; exit 1)
    "Syntax: ";
  if !ccharstr <> "" then (commandc := String.get !ccharstr 0);
  if !nick = "" then (Arg.usage speclist "Missing -nick."; exit 1);
  if !host = "" then (Arg.usage speclist "Missing -server."; exit 1);
  if !channel = "" then (Arg.usage speclist "Missing -channel."; exit 1);
  let host,port,username,realname,nick = !host,!port,!nick,!nick,!nick in
  begin try
    C.connect_by_name ~server:host ~port ~username ~mode:0 ~realname ~nick ()
    with _ -> None 
  end
  >>= function | None -> print_endline "Failed to connect."
               | Some connection -> print_endline ("Connecting to " ^ host)
  >>= fun () -> C.listen ~connection ~callback
  >>= fun () -> C.send_quit ~connection

let () = main ()
