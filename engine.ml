include Commandhub

open Util
open Gamestate

(* State of the game engine. *)
type engine_status =
  | Open    (* Waiting for players to join, game not yet started *)
  | InGame of Gamestate.t  (* Game in progress *)

let status : engine_status ref = ref Open

(* List of players who have joined the open game.
 * Invariant: [players] is empty when [status = InGame]. No duplicates. *)
let players : string list ref = ref []

let ai_names = ["Susie"; "John"; "Rob"; "Maxine"; "Wilbur"; "Thomas";
                "Wolf"; "Hilbert"; "Russell"; "Donald"; "Ben"; "Joe";
                "Carl"; "Karl"; "Pig"; "Dusty"; "Nugget"; "Seth"; "Rainer";
                "Magnus"; "Vince"; "Taylor"; "Junior"; "Samantha"; "Mango"]


let () = Random.self_init ()

let random_name () = List.nth ai_names (Random.int (List.length ai_names))

(* Does AI stuff on gamestate [gs]. *)
let rec perform_ai connection target gs =
  if (current_player gs).[0] = '*' && winner gs = None then
    let gs', message = Ai.decide2 gs in
    Unix.sleep 2;
    C.send_privmsg ~connection ~target ~message;
    perform_ai connection target gs'
  else
    gs

(* Commands logic start here.
 * No more declarations after this: *)
let () =

admin command "quit"
"Forces the bot to quit."
begin fun connection chan nick rest ->
  C.send_quit ~connection
  |> fun () -> exit 0
end;

command "rules"
"Gets a link to the rules."
begin fun connection chan target rest ->
  let message = "http://pastebin.com/n9jxPD50" in
  C.send_privmsg ~connection ~target ~message
end;

command "help"
"Get command list and command info: !help {command}"
begin fun connection chan nick rest ->
  let fst' (a,d,_,_) = (a,d) in
  let cmdis = (List.map fst' !registered_callbacks) |> remove_dups in
  let cmdnames = (List.map fst cmdis) |> remove_dups in
  let cmds = String.concat ", " cmdnames in
  let message = if List.mem_assoc (String.trim rest) cmdis then
    List.assoc (String.trim rest) cmdis
  else
    "Commands: " ^ cmds
  in
  C.send_privmsg ~connection ~target:chan ~message
end;

command "admins"
"Get a list of admins online."
begin fun connection chan nick rset ->
  let message = String.concat ", " !admins in
  C.send_privmsg ~connection ~target:chan ~message
end;

command "botinfo"
"Gets bot info."
begin fun connection chan nick rest ->
  "Version 0.0.2"
  |> fun m -> C.send_privmsg ~connection ~target:chan ~message:m
end;

useronly command "whoami"
"Gets who the user is."
begin fun connection chan nick rest ->
  C.send_privmsg ~connection ~target:chan ~message:nick
end;

admin command "whoami"
"Gets who the user is."
begin fun connection chan nick rest ->
  let message = "Admin " ^ nick in
  C.send_privmsg ~connection ~target:chan ~message
end;

command "say"
"!say {msg} causes the bot to say [msg]."
begin fun connection chan nick message ->
  C.send_privmsg ~connection ~target:chan ~message
end;

command "whereami"
"Outputs the current channel."
begin fun connection chan nick rest ->
  C.send_privmsg ~connection ~target:chan ~message:chan
end;

hook
begin fun connection -> let open Irc_message in
function
  | {prefix=Some p; command=NICK n} ->
      let old = parse_nick p in
      (if List.mem old !players then
        players := List.map (fun s -> if s = old then n else s) !players);
      (match !status with | Open -> () | InGame gs ->
        status := InGame (rename_player gs old n))
  | _ -> ()
end;

command "players"
"Gets the list of people in game."
begin fun connection chan nick rset ->
  match !status with
  | Open ->
      let message = String.concat ", " !players in
      if message <> "" then
      C.send_privmsg ~connection ~target:chan ~message
  | InGame gs ->
      let message = String.concat ", " (player_names gs) in
      C.send_privmsg ~connection ~target:chan ~message
end;

command "join"
"Allows you to join a game that has not yet started."
begin fun connection chan nick rest ->
  match !status with
  | Open ->
      if List.mem nick !players then
        let message = nick ^ " is already in the game." in
        C.send_privmsg ~connection ~target:nick ~message
      else begin
        players := nick :: !players;
        let message = nick ^ " has joined the game." in
        C.send_privmsg ~connection ~target:chan ~message
      end
  | InGame _ ->
      let message = "This game is already in progress." in
      C.send_privmsg ~connection ~target:nick ~message
end;

command "leave"
"Leaves a game."
begin fun connection chan nick rest ->
  match !status with
  | Open ->
      if List.mem nick !players then
        let rec remove a = function
          | [] -> []
          | h :: t -> if h = a then t else h :: (remove a t) in
        players := remove nick !players;
        C.send_privmsg ~connection ~target:chan ~message:(nick ^ " has left the game.")
      else
        C.send_privmsg ~connection ~target:nick ~message:(nick ^ " is not in the game.")
  | InGame gs ->
      if List.mem nick (player_names gs) then begin
        let gs = remove_player_and_reset gs nick in
        status := InGame gs;
        if List.length (player_names gs) < 2 then begin
          status := Open;
          C.send_privmsg ~connection ~target:chan ~message:"Game end."
        end else begin
          let message = nick ^" has left the game.  Round reset." in
          C.send_privmsg ~connection ~target:chan ~message;
          let after_ai = perform_ai connection chan gs in
          if winner after_ai <> None then
            status := Open
          else
            status := InGame (after_ai)
        end
      end
end;

command "start"
"Starts a game."
begin fun connection chan nick rest ->
  let all_cpus = List.for_all (fun s -> s.[0] = '*') in
  match !status with
  | Open ->
      if List.mem nick !players || all_cpus !players then begin
        (try status := InGame (new_game !players)
        with
          | NotEnoughPlayers ->
              let message = "Need at least 2 players to start." in
              C.send_privmsg ~connection ~target:chan ~message;
        );
        match !status with
        | Open -> ()
        | InGame gs ->
            let do_msg pname =
              if pname.[0] <> '*' then
                let message = "Your dice: " ^ dice_string gs pname in
                C.send_privmsg ~connection ~target:pname ~message
            in
            List.iter do_msg !players;
            players := [];
            let message = "The game has now started." in
            let message = message ^ " It is " ^ current_player gs ^ "'s" in
            let message = message ^ " turn to move." in
            C.send_privmsg ~connection ~target:chan ~message;
            let after_ai = perform_ai connection chan gs in
            if winner after_ai <> None then
              status := Open
            else
              status := InGame (after_ai)
      end else
        C.send_privmsg ~connection ~target:chan ~message:"You aren't joined!"
  | InGame _ ->
      C.send_privmsg ~connection ~target:chan ~message:"Already in game."
end;

command "joinai"
"Causes an AI player to join."
begin fun connection chan nick rest ->
  match !status with
  | Open ->
      let ai_name = "*"^(random_name ()) in
      players := ai_name :: !players;
      let message = ai_name ^ " has joined the game." in
      C.send_privmsg ~connection ~target:chan ~message
  | InGame _ ->
      let message = "This game is already in progress." in
      C.send_privmsg ~connection ~target:nick ~message
end;

command "guess"
"Performs a guess.  Syntax:  !guess n m"
begin fun connection chan nick rest ->
  let parsed_guess rest =
    let splitted = split rest in
    if List.length splitted < 2 then
      let message = "Incorrect syntax.  Do !help guess for help." in
      C.send_privmsg ~connection ~target:nick ~message;
      None
    else
      let n,m = List.hd splitted, List.hd (List.tl splitted) in
      try let n,m = int_of_string n, int_of_string m in Some (n,m)
      with _ ->
        let message = "Incorrect syntax.  n and m must be integers." in
        C.send_privmsg ~connection ~target:nick ~message;
        None
  in
  match !status with
  | Open -> C.send_privmsg ~connection ~target:nick ~message:"No game is running."
  | InGame gs ->
      if current_player gs <> nick then
        C.send_privmsg ~connection ~target:chan ~message:"It isn't your turn."
      else
        match parsed_guess rest with
        | None -> ()
        | Some (n,m) ->
            let gs', message = estimate gs n m in
            status := InGame gs';
            C.send_privmsg ~connection ~target:chan ~message;
            status := InGame (perform_ai connection chan gs')
end;

admin command "showalldice"
"Shows all dice."
begin fun connection target nick rest ->
  match !status with
  | Open -> ()
  | InGame gs ->
      let message = to_string gs in
      C.send_privmsg ~connection ~target ~message
end;


command "dice"
"Shows your dice."
begin fun connection target nick rest ->
  match !status with
  | Open -> C.send_privmsg ~connection ~target:nick ~message:"No game is running."
  | InGame gs ->
      let message = dice_string gs nick in
      if message <> "" then
        C.send_privmsg ~connection ~target:nick ~message
      else
        C.send_privmsg ~connection ~target:nick ~message:"You aren't in game."
end;

command "call"
"This means you doubt the last guess.  Ends the round."
begin fun connection target nick rest ->
  match !status with
  | Open -> C.send_privmsg ~connection ~target:nick ~message:"No game is running."
  | InGame gs ->
      if current_player gs <> nick then
        C.send_privmsg ~connection ~target:nick ~message:"It's not your turn."
      else
        let gs',message = call gs in
        status := InGame gs';
        C.send_privmsg ~connection ~target ~message;
        if winner gs' <> None then
          status := Open
        else begin
          let send_msg p = if p.[0] <> '*' then
            let message = "Your dice: " ^ dice_string gs' p in
            C.send_privmsg ~connection ~target:p ~message
          in
          List.iter send_msg (player_names gs');
          let after_ai = (perform_ai connection target gs') in
          if winner after_ai <> None then
            status := Open
          else
            status := InGame after_ai
        end
end;

command "spoton"
"This means you think the last guess is exactly correct.  Ends the round."
begin fun connection target nick rest ->
  match !status with
  | Open -> C.send_privmsg ~connection ~target:nick ~message:"No game is running."
  | InGame gs ->
      if current_player gs <> nick then
        C.send_privmsg ~connection ~target:nick ~message:"It's not your turn."
      else
        let gs',message = spoton gs in
        status := InGame gs';
        C.send_privmsg ~connection ~target ~message;
        if winner gs' <> None then
          status := Open
        else begin
          let send_msg p = if p.[0] <> '*' then
            let message = "Your dice: " ^ dice_string gs' p in
            C.send_privmsg ~connection ~target:p ~message
          in
          List.iter send_msg (player_names gs');
          let after_ai = (perform_ai connection target gs') in
          if winner after_ai <> None then
            status := Open
          else
            status := InGame after_ai
        end
end;

command "status"
"Gets the game status."
begin fun connection target nick rest ->
  match !status with
  | Open -> C.send_privmsg ~connection ~target:nick ~message:"No game is running."
  | InGame gs ->
      C.send_privmsg ~connection ~target ~message:(game_string gs)
end;

command "instructions"
"Shows a link where you can get the game instructions."
begin fun connection target nick rest ->
  C.send_privmsg ~connection ~target:nick ~message:"Instructions can be found here: http://pastebin.com/n9jxPD50."
end;
