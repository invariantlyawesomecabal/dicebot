open Util

type player = string * int list

exception NotEnoughPlayers

let () = Random.self_init ()

(* An estimate of (0, 0) indicates the beginning of a new round. *)
type t = { players : player array;
           current_player : int;
           estimate : int*int }


(* The starting number of dice each player receives. *)
let ndice_start = 5

(* [roll_n_dice n] is a list of the values generated when n dice are rolled. *)
let rec roll_n_dice n = if n = 0 then []
                        else (Random.int(6) + 1) :: (roll_n_dice (n-1))

(* [higher_than a b] determines if the pair b is higher than a*)
let higher_than (n1,n2) (n1',n2') =
  let effective_n1 = if n2 = 1 then n1 * 2 else n1 in
  let effective_n1' = if n2' = 1 then n1' * 2 else n1' in
  effective_n1' > effective_n1 || (effective_n1' = effective_n1 && n2' > n2)

let valid (n,m) =
  n >= 1 && m >= 1 && m <= 6

let new_game pnames =
  if (List.length pnames) < 2 then raise NotEnoughPlayers
  else 
    let players = Array.init (List.length pnames) 
                  (fun i -> (List.nth pnames i, roll_n_dice ndice_start)) in
    { players; current_player = 0 ; estimate = (0, 0) }

let current_player gs = fst gs.players.(gs.current_player)

let current_dice gs = snd gs.players.(gs.current_player)

let current_estimate gs = gs.estimate

let player_names gs = Array.map fst gs.players |> Array.to_list

let all_dice gs = Array.fold_left (fun l plr -> l @ (snd plr)) [] gs.players

let num_dice gs = List.length (all_dice gs)

let roll_dice gs =
  let roll_player_dice (name, dice) =
    (name, roll_n_dice (List.length dice)) in
  {gs with players = Array.map roll_player_dice gs.players}

let winner gs = match gs.players with
                | [|(name, _)|] -> Some name
                | _ -> None

let format_estimate (n,m) = 
  string_of_int n^" "^string_of_int m^"'s"

(* [estimate gs n m] is the game [gs] after the current player 
 * makes an estimate "n m's" and a message to be broadcasted to the players. *)
let estimate : t -> int -> int -> t * string = fun gs n m ->
  if valid (n,m) && higher_than gs.estimate (n, m) then 
    let the_guy = current_player gs in
    let current_player = (gs.current_player + 1) mod Array.length gs.players in
    let msg = the_guy ^ " guesses " ^ format_estimate (n,m) ^ "." in
    {gs with current_player; estimate = (n, m)}, msg
  else if valid (n,m) then
    gs, ("Invalid guess. "^ format_estimate (n,m) ^
        "  Must be higher than: " ^ format_estimate gs.estimate)
  else
    gs, ("Invalid guess " ^ format_estimate (n,m) ^ 
         " (n,m): m must be between 1 and 6 and n must be > 0.")

(* [remove_player gs player] is [gs] with [player] removed. *)
let remove_player : t -> string -> t = fun gs player ->
  let players' = gs.players
  |> Array.to_list
  |> List.filter (fun (s,d) -> s <> player)
  |> Array.of_list in
  {gs with players = players'}

let remove_player_and_reset : t -> string -> t = fun gs player ->
  let gs' = remove_player gs player in
  {gs' with current_player=0; estimate = (0,0)}

(* [call gs] is the game [gs] after the current player makes a call and a message to be broadcasted to the players. *)
let call : t -> t * string = fun gs ->
  if gs.estimate = (0,0) then
    gs, "There is nothing to call!"
  else
    (* enough = true when the estimate was accurate *)
    let count_this = fun x -> x = snd gs.estimate || x = 1 in
    let num = List.length (List.filter count_this (all_dice gs)) in
    let enough = num >= fst gs.estimate in
    let gs = roll_dice gs in
    let gs = {gs with estimate = (0,0)} in
    let loser = if enough then
      gs.current_player
    else
      let mdlus = Array.length gs.players in
      (gs.current_player + mdlus - 1) mod mdlus
    in
    let nam, dice = gs.players.(loser) in
    let result = current_player gs ^ " calls. " in
    let result = result ^ "There were " ^ string_of_int num ^ " total. " in
    if List.length dice > 1 then begin
      gs.players.(loser) <- (nam, List.tl dice);
      ({gs with current_player = loser}, result ^ nam ^ " has lost a dice.")
    end else
      let gs' = remove_player gs nam in
      let loser' = loser mod Array.length gs'.players in
      let gs'' = {gs' with current_player = loser'} in
      let msg = match winner gs'' with
        | Some person -> " " ^ person ^ " has won the game."
        | None -> ""
      in
      (gs'', result ^ nam ^ " is out of the game." ^ msg)

(* [spoton gs] is the game [gs] after the current player makes a spot-on and a message to be broadcasted to the players. *)
let spoton : t -> t * string = fun gs ->
  if gs.estimate = (0,0) then
    gs, "There is nothing to call!"
  else
    (* enough = true when the estimate was accurate *)
    let count_this = fun x -> x = snd gs.estimate || x = 1 in
    let num = List.length (List.filter count_this (all_dice gs)) in
    let enough = num = fst gs.estimate in
    let gs = roll_dice gs in
    let gs = {gs with estimate = (0,0)} in
    let nam, dice = gs.players.(gs.current_player) in
    let result = current_player gs ^ " calls spot-on. " in
    let result = result ^ "There were " ^ string_of_int num ^ " total. " in
    if enough then begin
      gs.players.(gs.current_player) <- (nam, 1::dice);
      (gs, result ^ nam ^ " has gained a dice.")
    end else if List.length dice > 1 then begin
      gs.players.(gs.current_player) <- (nam, List.tl dice);
      (gs, result ^ nam ^ " lost a dice.")
    end else
      let gs' = remove_player gs nam in
      let cur_player = gs.current_player mod Array.length gs'.players in
      let gs'' = {gs' with current_player = cur_player} in
      let msg = match winner gs'' with
        | Some person -> " " ^ person ^ " has won the game."
        | None -> ""
      in
      (gs'', result ^ nam ^ " is out of the game." ^ msg)


(* [rename_player player newname] is [gs] with [player] renamed with [newname]. *)
let rename_player : t -> string -> string -> t = fun gs nam newnam ->
  let renam n n' (name, d) = if name = n then (n', d) else (name, d) in
  {gs with players = Array.map (renam nam newnam) gs.players}


(* [game_string gs] is a string representation of [gs] *)
let game_string : t -> string = fun gs ->
  let s = ref "" in
  let f (name, dice) = 
    s := !s ^ name ^ ": " ^ (string_of_int (List.length dice)) ^ "  " in
  Array.iter f gs.players;
  s := "Current guess: " ^ format_estimate gs.estimate ^ "  " ^ !s;
  "Current player: " ^ current_player gs ^ "  " ^ !s

let int_list_to_string l = String.concat ", " (List.map string_of_int l)

(* [dice_string gs player] is a string representation of [player]'s dice. *)
let dice_string : t -> string -> string = fun gs player ->
  match List.filter (fun (s,d) -> s = player) (Array.to_list gs.players) with
  | (_,d)::_ -> int_list_to_string d
  | [] -> ""

(* [to_string gs] is a string representation of [gs] for debugging purposes. *)
let to_string : t -> string = fun gs ->
  let s = ref "" in
  let f (name,dice) = s := !s ^ name ^ ": " ^ int_list_to_string dice ^ "  " in
  Array.iter f gs.players;
  !s
