open Util

module C = Irc_client_unix

type user_permissions = {admin : bool; owner: bool; user: bool}
type callback = C.connection_t -> string -> string -> string -> unit
type irc_callback = C.connection_t -> Irc_message.t -> unit
type cmd_callback = string * string * callback * user_permissions

let registered_callbacks : cmd_callback list ref = ref []
let registered_handlers : irc_callback list ref = ref []

let admins = ref []

let hook (cb:irc_callback) : unit = 
  registered_handlers := cb::!registered_handlers

type commandfn_type = string -> string -> callback -> unit

let command ?perms:(p={admin=true; owner=true; user=true}) 
            (comd:string) (docs:string) (cb:callback) : unit = 
  let cb' = fun conn chan nick rest ->
    let nick = parse_nick nick in
    if p.user && not(List.mem nick !admins) then
      cb conn chan nick rest
    else if p.admin && List.mem nick !admins then
      cb conn chan nick rest
  in
  registered_callbacks := (comd, docs, cb', p)::!registered_callbacks

let admin (f : commandfn_type) : commandfn_type =
  (fun s d c -> command s d c ~perms:{admin=true; owner=true; user=false})

let owner (f : commandfn_type) : commandfn_type = 
  (fun s d c -> command s d c ~perms:{admin=false; owner=true; user=false})

let useronly (f : commandfn_type) : commandfn_type =
  (fun s d c -> command s d c ~perms:{admin=false; owner=false; user=true})

let () = hook 
begin fun connection ->
function
  | {Irc_message.command=Irc_message.Other ("353", s); _} -> 
      let startswith c s = s.[0] = c in
      List.iter print_endline (split (List.hd (List.rev s)));
      admins := List.filter (startswith '@') (split (List.hd (List.rev s)));
      let strtail s = String.sub s 1 (String.length s - 1) in
      admins := List.map strtail !admins 
  | {Irc_message.prefix=Some p; command=Irc_message.NICK n} -> 
      let old = parse_nick p in
      if List.mem old !admins then
        admins := List.map (fun s -> if s = old then n else s) !admins
  | _ -> ()
end
