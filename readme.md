The OPAM package irc-client is required for compilation.
**IMPORTANT!**  irc-client version 0.3.0 is required.  Older versions will fail.
To compile, simply run ./compile

To run, simply edit ./run with your own personalized nickname and IRC server

(The IRC Servers chat.freenode.net and irc.snoonet.org are recommended)

and then execute the file ./run.
