(* most basic AI bot, simply gives a higher estimate than last one*)
let decide (gs:Gamestate.t) : Gamestate.t * string =
  let n, m = Gamestate.current_estimate gs in
  let n = if m = 1 then 2*n else n in
  Gamestate.estimate gs (n+1) ((m+1) mod 6 + 1)

let rec num_of_val v lst count =
  match lst with
  |[] -> count
  |h::tl -> if h = v then num_of_val v tl (count+1)
                     else num_of_val v tl count

let rec fact x = if x = 0 then 1 else x*(fact (x-1))

let rec calc_prob k n is_ones =
  if(k < 0 || k > n) then 0.
  else let nk = (float_of_int (fact n))
                /.(float_of_int ((fact k)*(fact (n-k)))) in
       let p = if is_ones then 1.0/.6.0 else 1.0/.3. in
       let prb = (p ** (float_of_int k))*.((1.0-.p)**(float_of_int (n-k)))
                 *.nk in
       prb+.(calc_prob (k-1) n is_ones)

let decide2 (gs:Gamestate.t) : Gamestate.t * string =
  let ndice = Gamestate.num_dice gs in
  let n, m = Gamestate.current_estimate gs in
  let mydice = Gamestate.current_dice gs in
  let nov = num_of_val m mydice 0 in
  (* if probability that there are less than n m's is greater than
   * a particular amount, it's probably wrong, so the AI calls *)
  if (calc_prob (n-nov-1) (ndice-(List.length mydice)) (m=1)) > 0.7 then
    Gamestate.call gs
  (* if AI has more than n m's, it makes an estimate of 1 more m*)
  else if nov > n then
    Gamestate.estimate gs (n+1) m
  (* AI opts to raise dice number rather than number of dice, as
     this is generally a safer bet in early stages of game *)
  else if m < 6 then
    let n' = if n = 0 then 1 else n in
    Gamestate.estimate gs (if m = 1 then 2*n' else n') (m+1)
  (* With no other option, AI raises n by 1*)
  else Gamestate.estimate gs (n+1) m
