let parse_nick nickstr =
  let open String in
  String.sub nickstr 0 (String.index nickstr '!')


(** [remove_dups l] returns [l] with duplicates removed.
    Preserves order of all the elements *)
let remove_dups (l : 'a list) : 'a list = 
  let rec inner acc = function
    | [] -> List.rev acc
    | x::xs when List.mem x acc -> inner acc xs
    | x::xs -> inner (x::acc) xs
  in inner [] l


let rec split s = let s = String.trim s in
  if not (String.contains s ' ') then [s] else
  if s = "" then [] else
  let i = String.index s ' ' in
  (String.sub s 0 i)::split (String.sub s (i + 1) ((String.length s) - i - 1))
